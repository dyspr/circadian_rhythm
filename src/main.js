var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var numOfRects = 56
var rects = []

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < numOfRects; i++) {
    rects.push(new Rectangle())
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < numOfRects; i++) {
    rects[i].display()
    rects[i].grow()
    if (rects[i].isAtLimit() === true) {
      rects.splice(i, 1)
    }
    setTimeout(function(){
      rects.push(new Rectangle())
    }, Math.random() * 100)
  }
}

class Rectangle {
  constructor() {
    this.sides = []
    for (var i = 0; i < 4; i++) {
      this.sides.push(Math.floor(Math.random() * 2))
    }
    this.limit = 0.6
    this.size = 0.05 + 0.05 * Math.random()
    this.dir = Math.PI * 2 * Math.random()
    this.distance = Math.random() * this.limit * 0.5
    this.posX = this.distance * sin(this.dir)
    this.posY = this.distance * cos(this.dir)
    this.maxSize = 0.2
    this.start = Math.random() * Math.PI * 2
    this.end = Math.random() * Math.PI * 2
  }

  display() {
    stroke(255)
    strokeCap(PROJECT)
    strokeWeight(boardSize * 0.01 - this.size * 0.05 * boardSize)
    noFill()
    arc(windowWidth * 0.5 + this.posX * boardSize, windowHeight * 0.5 + this.posY * boardSize, this.size * boardSize, this.size * boardSize, this.start + this.size * boardSize * 0.05, this.end + this.size * boardSize * 0.05, CHORD)
  }

  grow() {
    this.size += 0.001
    this.start += Math.random() * 0.5
    this.end += Math.random() * 0.5
  }

  isAtLimit() {
    if (this.size > this.maxSize) {
      return true
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
